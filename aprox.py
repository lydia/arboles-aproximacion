#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    for trees in range(min_trees, max_trees):
        production = compute_trees(trees)
        production.append((trees, production))
    return productions

def read_arguments():
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        sys.exit("all arguments must be integrers")
    except IndexError:
        sys.exit("you must put 5 integrers")
    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees
    global fruit_per_tree
    global reduction
    best_production = 0
    best_trees = 0
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    for treesp in productions:
        trees = treesp[0]
        production = treesp[1]
        print(trees, production)
        if production > best_production:
            best_trees = trees
            best_production = production
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
